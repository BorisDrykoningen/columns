#include "algorithms.h"

#include <assert.h>
#include <stdint.h>


void grid_apply_gravity(struct grid* self) {
    // The algorithm is applied column after column
    for (size_t col = 0; col < self->width; ++col) {
        const size_t height = grid_column_height(self, col);
        // The next position of the current cell
        size_t next = 0;

        for (size_t row = 0; row < height; ++row) {
            const int color = grid_get(self, row, col);
            if (!color) {
                // The cell is empty, nothing to do... for now
                continue;
            }

            // We just copy the current color to its new location (next, col).
            // After that, it will be overwritten by the following bricks. If
            // not, it is too much near to the end so we will take care of it
            const int status = grid_set(self, next, col, color);
            // It's impossible for the insertion to fail
            assert(!status);

            // As the current brick fills (next, col), we update next to match
            // the cell above
            ++next;
        }

        // We remove the bricks that have not been overwritten
        for (size_t row = next; row < height; ++row) {
            grid_remove(self, row, col);
        }
    }
}


struct cell_ref {
    size_t row;
    size_t col;
};


enum direction {
    DIRECTION_UP,
    DIRECTION_UP_LEFT,
    DIRECTION_LEFT,
    DIRECTION_DOWN_LEFT,
    DIRECTION_DOWN,
    DIRECTION_DOWN_RIGHT,
    DIRECTION_RIGHT,
    DIRECTION_UP_RIGHT,
    // The first direction to choose
    DIRECTION_FIRST = DIRECTION_UP,
    // The last one
    DIRECTION_MAX_ENUM = DIRECTION_UP_RIGHT,
};


struct dfs_stack_element {
    struct cell_ref pos;
    enum direction next;
};


// Writes its result to res, returns whether the new position is valid
static bool grid_step(const struct grid* grid, struct cell_ref* res, const struct cell_ref* origin, enum direction direction) {
    // We fill with default values
    res->row = origin->row;
    res->col = origin->col;

    // Then, we alter these values according to the direction
    if (direction == DIRECTION_UP || direction == DIRECTION_UP_LEFT || direction == DIRECTION_UP_RIGHT) {
        res->row = origin->row + 1;
    }

    if (direction == DIRECTION_DOWN || direction == DIRECTION_DOWN_LEFT || direction == DIRECTION_DOWN_RIGHT) {
        if (origin->row == 0) {
            return false;
        }
        res->row = origin->row - 1;
    }

    if (direction == DIRECTION_LEFT || direction == DIRECTION_UP_LEFT || direction == DIRECTION_DOWN_LEFT) {
        if (origin->col == 0) {
            return false;
        }
        res->col = origin->col - 1;
    }

    if (direction == DIRECTION_RIGHT || direction == DIRECTION_UP_RIGHT || direction == DIRECTION_DOWN_RIGHT) {
        if (origin->col == grid->width - 1) {
            return false;
        }
        res->col = origin->col + 1;
    }

    return true;
}


// Writes result to direction, returns if direction is valid
static bool grid_next_direction(enum direction* direction) {
    // direction is in [0, DIRECTION_MAX_ENUM]
    int as_int = *direction;
    ++as_int;

    // If we go past DIRECTION_MAX_ENUM, then direction is no longer valid. We
    // have exhausted all the different possible directions
    if (as_int > DIRECTION_MAX_ENUM) {
        return false;
    }
    *direction = as_int;
    return true;
}


// We assume n was already valid
static bool sum_still_valid(size_t n, int m, size_t max) {
    // Okay, that's a lot of functions, but bounds-checking without trigerring
    // overflows, underflows and properly handling integer promotion is either
    // painful or verbose. I chose the verbose way
    if (m < 0) {
        // n + m > 0
        // <=> n > 0 - m
        // <=> n > -m
        return n >= (size_t) -m;
    }
    if (m > 0) {
        // n + m <= max
        // <=> m <= max - n
        return (size_t) m <= max - n;
    }
    // n is assumed to be valid, n + 0 is still valid
    return true;
}


bool is_valid_step(const struct grid* grid, size_t row, size_t col, int voffset, int hoffset) {
    return sum_still_valid(row, voffset, SIZE_MAX)
        && sum_still_valid(col, hoffset, grid->width - 1);
}


// Doesn't actually mark (row, col). Only returns a positive value if it needs
// to be marked. This is for avoiding checking up to 4 times whether (row, col)
// is marked
static int grid_mark_match(const struct grid* self, struct array* marked, size_t row, size_t col, int vstep, int hstep) {
    const int color = grid_get(self, row, col);

    if (is_valid_step(self, row, col, -vstep, -hstep) && grid_get(self, row - vstep, col - hstep) == color){
        // The previous element is colored the same way. Thus, we should call
        // this function from the previous element (or sooner) to avoid
        // duplicate reports of the same match.
        // Note the previous cell is (row - vstep, col - hstep)
        return 0;
    }

    // Going to the next cell while the next cell has the good color
    size_t count = 0;
    while (
        is_valid_step(self, row, col, vstep * (count + 1), hstep * (count + 1)) &&
        grid_get(self, row + vstep * (count + 1), col + hstep * (count + 1)) == color
    ) {
        ++count;
    }
    // At this point, count is the number of valid moves. We need the number of
    // aligned cells of the same color so we add 1
    ++count;

    if (count < 3) {
        // Nothing to mark, we just return 0
        return 0;
    }

    for (size_t i = 1; i < count; ++i) {
        const struct cell_ref current = {row + vstep * i, col + hstep * i};
        if (array_find(marked, &current) >= array_size(marked)) {
            // The current cell is not marked, let's mark it
            if (array_push_back(marked, &current)) {
                return -1;
            }
        }
    }

    return count - 2;
}


// Here is a chonky boi
static int grid_break_matches(struct grid* self, size_t row_start, size_t col_start) {
    // There is clearly not a good way to destroy matches one after one without
    // destroying a brick that would be in another match. Thus, we destroy them
    // set by set. I guess the best way to do this is to see it as a graph,
    // where neighbors are just adjacent cells with the same color.
    // Here, we will use Depth First Search to implement walking through that
    // graph. We will use
    // - an array as the stack
    // - an array to mark vertrices to remove (because there are aligned cells)
    // - an array to mark vertrices already visited
    // Indeed, as long as we avoid cycles, which may be implemented by searching
    // through the stack, we can tell the algorithm will finally end. But we may
    // walk through roughly n² vertrices. By searching through the stack to
    // detect cycles, we have a O(n³) complexity. If we only search through the
    // visited vertrices, we search through roughly n vertrices, but each vertex
    // is visited only once! Thus, we have a O(n²) complexity
    // ---------------
    // Here, n is the number of vertrices in a "stain" of the same color. It is
    // indeed, proportional to the number of vertrices in the grid, because as
    // the gravity creates new stains, stains may get bigger than possible
    // otherwise.

    const int color = grid_get(self, row_start, col_start);
    int score = 0;

    // The call stack, used for implementing Depth First Search
    struct array stack;
    // The marked vertrices that will be removed in the end. I avoid duplicates
    // to keep the array small, as it doesn't affect time complexity (amortised
    // O(n) for each vertex, which is O(n²) besides an O(n²) walk in a graph)
    // but does affect space complexity
    struct array marked;
    // The visited vertrices
    // TODO: reduce time complexity of the whole function by linearizing this
    // for O(1) random access instead of searching in a whole array. Such a
    // change would also affect the above array, which would contain duplicates.
    // Otherwise, time complexity wouldn't change
    struct array visited;
    array_create(&stack, sizeof(struct dfs_stack_element));
    array_create(&marked, sizeof(struct cell_ref));
    array_create(&visited, sizeof(struct cell_ref));

    // Walking the graph
    struct dfs_stack_element next = {{row_start, col_start}, DIRECTION_FIRST};
    if (array_push_back(&stack, &next) || array_push_back(&visited, &next)) {
        goto walk_error;
    }
    while (!array_is_empty(&stack)) {
        // We retreive data from the stack. Where are we? Where do we go?
        struct dfs_stack_element* top_of_stack = array_get(&stack, array_size(&stack) - 1);

        if (top_of_stack->next == DIRECTION_FIRST) {
            // This is the first time we handle that cell

            // We scan for matches
            const int horizontal_status = grid_mark_match(self, &marked, top_of_stack->pos.row, top_of_stack->pos.col,  0, 1);
            const int vertical_status =   grid_mark_match(self, &marked, top_of_stack->pos.row, top_of_stack->pos.col,  1, 0);
            const int tl2br_status =      grid_mark_match(self, &marked, top_of_stack->pos.row, top_of_stack->pos.col, -1, 1);
            const int bl2tr_status =      grid_mark_match(self, &marked, top_of_stack->pos.row, top_of_stack->pos.col,  1, 1);

            if (horizontal_status < 0 || vertical_status < 0 || tl2br_status < 0 || bl2tr_status < 0) {
                goto mark_error;
            }

            // We may mark the current cell (the other ones have already been
            // marked)
            if (
                (horizontal_status || vertical_status || tl2br_status || bl2tr_status) &&
                array_find(&marked, &top_of_stack->pos) >= array_size(&marked)
            ) {
                if (array_push_back(&marked, &top_of_stack->pos)) {
                    goto mark_error;
                }
            }

            // We update the score
            score += horizontal_status + vertical_status + tl2br_status + bl2tr_status;
        }

        // We walk to the next cell
        const bool step_successful = grid_step(self, &next.pos, &top_of_stack->pos, top_of_stack->next);
        const bool all_next_consumed = !grid_next_direction(&top_of_stack->next);

        if (step_successful) {
            // We have a cell to walk to
            const size_t num_visited = array_size(&visited);
            if (grid_get(self, next.pos.row, next.pos.col) == color && array_find(&visited, &next.pos) >= num_visited) {
                // This cell belongs to the stain and has not been visited
                next.next = DIRECTION_FIRST;
                if (array_push_back(&stack, &next) || array_push_back(&visited, &next.pos)) {
                    goto walk_error;
                }

                // Now that this cell is in the stack, it's the next visited
            }
        }
        // If step_successful is false, then we just try the next cell

        if (all_next_consumed) {
            // There is nothing after this one. Destacking
            array_pop_back(&stack);
        }
    }


    // Removing vertrices
    for (size_t i = 0; i < array_size(&marked); ++i) {
        const struct cell_ref ref = array_get_const_typed(&marked, i, struct cell_ref);
        grid_remove(self, ref.row, ref.col);
    }


    array_destroy(&visited);
    array_destroy(&marked);
    array_destroy(&stack);
    return score;

walk_error:
mark_error:
    array_destroy(&visited);
    array_destroy(&marked);
    array_destroy(&stack);
    return -1;
}


int grid_play(struct grid* self, const int bricks[3], size_t column) {
    // The score multiplier, called lambda
    int multiplier = 1;
    // The score computed this iteration. Is the difference between score's next
    // value and score's current value. multiplier doesn't affect score_inc,
    // only the final score
    int score_inc;
    // The accumulated score
    int score = 0;

    // We can insert our stack of bricks
    const size_t height = grid_column_height(self, column);
    if (grid_set(self, height,     column, bricks[0]) ||
        grid_set(self, height + 1, column, bricks[1]) ||
        grid_set(self, height + 2, column, bricks[2])
    ) {
        // Propagating insertions errors
        goto insert_error;
    }

    do {
        // We must reset this iteration's score
        score_inc = 0;

        grid_apply_gravity(self);

        // TODO: be smarter! We clearly test too much matches!
        for (size_t col = 0; col < self->width; ++col) {
            const size_t col_height = grid_column_height(self, col);
            for (size_t row = 0; row < col_height; ++row) {
                // We need to skip empty cells
                if (grid_get(self, row, col) == 0) {
                    continue;
                }

                // We destroy all the adjacent matches starting at this cell
                int status = grid_break_matches(self, row, col);
                if (status < 0) {
                    // Propagating grid_break_matches' errors
                    goto match_error;
                }

                // We accumulate the score
                score_inc += status;
            }
        }

        score += multiplier * score_inc;
        ++multiplier;
    } while (score_inc);

    return score;

match_error:
insert_error:
    return -1;
}

