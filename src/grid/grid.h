#ifndef SRC_GRID_GRID_H
#define SRC_GRID_GRID_H

#include "../array/array.h"

#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The grid of the columns game. Each column is encoded using an integer.
 * The empty cell is encoded using the special number 0
*/
struct grid {
    size_t width;
    // The capacity in terms of lines, not cells!
    size_t capacity;
    int* content;
    struct array heights;
};

/**
 * @brief Creates a grid
 * @param self the grid to create. Is treated as if left uninitialized
 * @param width the width of the grid. Once set, you can never change the grid's
 * width. Choose wisely
 * @return 0 in case of success, any other value in case of error. If a non-zero
 * value is returned, the grid is not created and may not be destroyed
*/
int grid_create(struct grid* self, size_t width);


/**
 * @brief Copies other into self
 * @param self the grid to create. Is treated as if uninitialized
 * @param other the grid to copy
 * @return 0 in case of success, any other value in case of error
*/
int grid_create_copy(struct grid* self, const struct grid* other);


/**
 * @brief Destroys a grid
 * @param grid the grid to destroy
*/
void grid_destroy(struct grid* self);


/**
 * @brief Places a colored brick in the grid. May grow the grid if necessary
 * @param line the index of the line, starting at the bottom and at 0
 * @param column the index of the column, starting at the left and at 0
 * @param color the color of the brick to put in the grid. Must be different
 * than 0
 * @return 0 in case of success, any other value in case of error
*/
int grid_set(struct grid* self, size_t line, size_t column, int color);


/**
 * @brief Gives the color of a cell
 * @param line the index of its line, starting at the bottom and at 0
 * @param column the index of its column, startint at the left and at 0
 * @return the color of that cell or 0 if empty. A cell that doesn't exist is
 * defined as empty
*/
int grid_get(const struct grid* self, size_t line, size_t column);


/**
 * @brief Removes a brick from the grid
 * @param line the index of its line, starting at the bottom and at 0
 * @param column the index of its column, startint at the left and at 0
*/
void grid_remove(struct grid* self, size_t line, size_t column);


/**
 * @brief Tells the height of a column
 * @param self the array to inspect
 * @param col the column number (starting at 0)
 * @return the height of the column
*/
size_t grid_column_height(const struct grid* self, size_t col);

#ifdef __cplusplus
}
#endif

#endif

