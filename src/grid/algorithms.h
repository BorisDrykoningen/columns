#ifndef SRC_GRID_ALGORITHMS_H
#define SRC_GRID_ALGORITHMS_H

#include "grid.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Applies gravity to the bricks of a grid, making them fall if there is
 * some available space below
 * @param self the grid to apply gravity on
*/
void grid_apply_gravity(struct grid* self);


/**
 * @brief Scans the grid to compute how much score the player will do
 * @param self the grid to play in. Will be altered. You may want to make a copy
 * for simulation purposes
 * @param bricks the bricks to place in the grid. Read from bottom to top
 * @param column the column to place the bricks on
 * @return the score if positive or null, an error code if negative
 * @see grid_create_copy
*/
int grid_play(struct grid* self, const int bricks[3], size_t column);

#ifdef __cplusplus
}
#endif

#endif

