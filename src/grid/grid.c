#include "grid.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define GRID_INITIAL_HEIGHT 4


int grid_create(struct grid* self, size_t width) {
    // Filling general data
    self->content = malloc(GRID_INITIAL_HEIGHT * width * sizeof(int));
    self->width = width;
    self->capacity = GRID_INITIAL_HEIGHT;

    // Filling the array with width 0s
    array_create(&self->heights, sizeof(size_t));
    const size_t zero = 0;
    for (size_t i = 0; i < width; ++i) {
        if (array_push_back(&self->heights, &zero)) {
            return -1;
        }
    }
    assert(array_size(&self->heights) == width);

    return self->content == NULL;
}


static size_t grid_index(const struct grid* self, size_t line, size_t column) {
    return self->width * line + column;
}


int grid_create_copy(struct grid* self, const struct grid* other) {
    self->width = other->width;
    self->capacity = other->capacity;
    self->content = malloc(self->capacity * self->width * sizeof(int));
    if (!self->content) {
        goto malloc_error;
    }

    // Copying content. First, we copy what we can in a large block
    size_t min_height = array_get_const_typed(&other->heights, 0, size_t);
    for (size_t i = 1; i < array_size(&other->heights); ++i) {
        const size_t height = array_get_const_typed(&other->heights, i, size_t);
        if (height < min_height) {
            min_height = height;
        }
    }
    memcpy(self->content, other->content, min_height * self->width * sizeof(int));

    // Then, some content may be surrounded by uninitialized memory so we don't
    // copy it
    for (size_t i = 0; i != array_size(&other->heights); ++i) {
        for (size_t j = min_height; j < array_get_const_typed(&other->heights, i, size_t); ++j) {
            // Can't use grid_set :(
            self->content[grid_index(self, j, i)] = grid_get(other, j, i);
        }
    }

    if (array_create_copy(&self->heights, &other->heights)) {
        goto array_copy_error;
    }
    return 0;

array_copy_error:
    free(self->content);
malloc_error:
    return -1;
}


void grid_destroy(struct grid* self) {
    array_destroy(&self->heights);
    free(self->content);
}


static int grid_reserve(struct grid* self, size_t new_height) {
    if (self->capacity >= new_height) {
        // We have enough memory, no need for more
        return 0;
    }

    size_t new_capacity = self->capacity * 2;
    if (new_height > new_capacity) {
        new_capacity = new_height;
    }

    int* new_content = realloc(self->content, self->width * new_capacity * sizeof(int));
    if (!new_content) {
        // In case of error, we return -1 and the grid is left unchanged
        return -1;
    }

    self->capacity = new_capacity;
    self->content = new_content;

    return 0;
}


int grid_set(struct grid* self, size_t line, size_t column, int color) {
    assert(color);
    assert(column < self->width);

    // We may need to realloc some memory
    const size_t col_height = grid_column_height(self, column);
    if (line >= col_height) {
        const size_t new_height = line + 1;
        if (grid_reserve(self, new_height)) {
            return -1;
        }

        // Now, the new lines are filled with garbage. Let's fix that
        for (size_t i = col_height; i < new_height; ++i) {
            self->content[grid_index(self, i, column)] = 0;
        }

        // Finally, logically growing up the grid
        array_get_typed(&self->heights, column, size_t) = new_height;
    }

    // We may need to update the individual height of the column
    if (array_get_const_typed(&self->heights, column, size_t) <= line) {
        array_get_typed(&self->heights, column, size_t) = line + 1;
    }

    // We just have to update the cell
    self->content[grid_index(self, line, column)] = color;
    return 0;
}


int grid_get(const struct grid* self, size_t line, size_t column) {
    assert(column < self->width);

    if (line >= array_get_const_typed(&self->heights, column, size_t)) {
        return 0;
    }

    return self->content[grid_index(self, line, column)];
}


void grid_remove(struct grid* self, size_t line, size_t column) {
    // We don't grow the grid to add an empty cell!
    if (line >= grid_column_height(self, column)) {
        return;
    }

    self->content[grid_index(self, line, column)] = 0;

    // Updating the height of that column
    size_t height = array_get_const_typed(&self->heights, column, size_t);
    while (height > 0 && grid_get(self, height - 1, column) == 0) {
        --height;
    }
    assert(grid_get(self, height, column) == 0);
    assert(height == 0 || grid_get(self, height - 1, column));

    array_get_typed(&self->heights, column, size_t) = height;
}


size_t grid_column_height(const struct grid* self, size_t col) {
    return array_get_const_typed(&self->heights, col, size_t);
}

