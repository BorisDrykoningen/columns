#ifndef SRC_ARRAY_ARRAY_H
#define SRC_ARRAY_ARRAY_H

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief A generic array type
*/
struct array {
    size_t size;
    size_t capacity;
    size_t item_size;
    // unsigned char* to enable pointer arithmetic
    unsigned char* data;
};


/**
 * @brief Initializes self as an empty array
 * @param self the array to initialize
 * @param item_size the size of an individual item in the array
*/
void array_create(struct array* self, size_t item_size);


/**
 * @brief Creates a deep copy of an array
 * @param self the array to initialize. Is treated as if uninitialized
 * @param other the array to copy
 * @return 0 in case of success, any other value in case of error
*/
int array_create_copy(struct array* self, const struct array* other);


/**
 * @brief Destroys self
 * @param self the array to destroy
*/
void array_destroy(struct array* self);


/**
 * @brief Tells the array's size
 * @return the array's size in number of objects
*/
size_t array_size(const struct array* self);


/**
 * @brief Tells if the array is empty
 * @return true if the array is empty, false otherwise
*/
bool array_is_empty(const struct array* self);


/**
 * @brief Gives access to the ith element
 * @param self the array to access
 * @param i the index of the element to get. Must exist
 * @return a void pointer to that element
*/
void* array_get(struct array* self, size_t i);


/**
 * @brief Gives access to the ith element in read-only
 * @param self the array to access
 * @param i the index of the element to get. Must exist
 * @return a void pointer to that element
*/
const void* array_get_const(const struct array* self, size_t i);


/**
 * @brief Gives access to the ith element
 * @param self the array to access
 * @param i the index of the element to get. Must exist
 * @param T the type of the element to access to
 * @return that element once dereferenced
*/
#define array_get_typed(self, i, T) (*((T*) array_get(self, i)))


/**
 * @brief Gives access to the ith element in read-only
 * @param self the array to access
 * @param i the index of the element to get. Must exist
 * @param T the type of the element to access to
 * @return that element once dereferenced
*/
#define array_get_const_typed(self, i, T) (*((const T*) array_get_const(self, i)))


/**
 * @brief Adds an element to the back of the array
 * @param self the array to add an element to
 * @param ptr a pointer to an object to copy and push in the array
 * @return 0 in case of success, any other value in case of failure
*/
int array_push_back(struct array* self, const void* ptr);


/**
 * @brief Inserts an element in the array
 * @param self the array to add an element to
 * @param i the index of that element after insertion
 * @param ptr a pointer to an object to copy and push in the array
 * @return 0 in case of success, any other value in case of failure
*/
int array_insert(struct array* self, size_t i, const void* ptr);


/**
 * @brief Removes the last element of the array
 * @param self the array to erase an element from
*/
void array_pop_back(struct array* self);


/**
 * @brief Removes an element from an array
 * @param self the array to erase an element from
 * @i the index of the element to remove
*/
void array_remove(struct array* self, size_t i);


/**
 * @brief Searches for a value in the array
 * @param self the array to search in
 * @param target the value to search
 * @return the index of that value if found, an invalid index (greater or equal
 * to array_size(self)) if not found
*/
size_t array_find(const struct array* self, const void* target);


#ifdef __cplusplus
}
#endif

#endif

