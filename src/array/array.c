#include "array.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>


#define INITIAL_CAPACITY 4

void array_create(struct array* self, size_t item_size) {
    self->size = 0;
    self->data = malloc(item_size * INITIAL_CAPACITY);
    self->capacity = self->data ? INITIAL_CAPACITY : 0;
    self->item_size = item_size;
}


int array_create_copy(struct array* self, const struct array* other) {
    self->item_size = other->item_size;
    self->size = other->size;
    self->capacity = other->capacity;
    self->data = malloc(self->item_size * self->capacity);
    if (!self->data) {
        return -1;
    }
    memcpy(self->data, other->data, self->size * self->item_size);
    return 0;
}


void array_destroy(struct array* self) {
    free(self->data);
}


size_t array_size(const struct array* self) {
    return self->size;
}


bool array_is_empty(const struct array* self) {
    return !self->size;
}


void* array_get(struct array* self, size_t i) {
    assert(i < self->size);
    return self->data + i*self->item_size;
}


const void* array_get_const(const struct array* self, size_t i) {
    // Safe because array_get doesn't actually modify self
    return array_get((struct array*) self, i);
}


static int array_reserve(struct array* self, size_t capacity) {
    if (capacity <= self->capacity) {
        // Nothing to do => nothing wrong
        return 0;
    }

    // We don't have the required capacity. Allocating more memory
    void* new_data = realloc(self->data, capacity * self->item_size);
    if (new_data) {
        self->data = new_data;
        self->capacity = capacity;
        return 0;
    }

    // Couldn't allocate more memory
    return 1;
}


static int array_grow_exp(struct array* self) {
    // We assume we need to increment the size of the array

    if (self->size == self->capacity) {
        // We can multiply the array's current capacity by two, which works most
        // of the time
        const size_t candidate_capacity = self->capacity * 2;

        // But there is the special case where twice the current capacity is not
        // enough: when capacity == 0 => 2*capacity == 0. In that case, we pick
        // the array's initial capacity for efficiency.
        // Note: the code below actually changes the allocation policy either if
        // the new capacity is not valid or if it would be innefficient
        const size_t actual_capacity = candidate_capacity < INITIAL_CAPACITY ? INITIAL_CAPACITY : candidate_capacity;

        if (array_reserve(self, actual_capacity)) {
            return 1;
        }
    }

    return 0;
}


int array_push_back(struct array* self, const void* ptr) {
    if (array_grow_exp(self)) {
        return 1;
    }

    // Whether we had enough memory from the beginning or the allocation was
    // successfull, now, we have enough memory
    assert(self->capacity > self->size);

    memcpy(self->data + self->size*self->item_size, ptr, self->item_size);
    ++self->size;

    return 0;
}


void array_pop_back(struct array* self) {
    if (self->size) {
        --self->size;
    }
}


int array_insert(struct array* self, size_t i, const void* ptr) {
    // i must end-up being a valid index
    assert(i <= self->size);

    // We may need more memory
    if (array_grow_exp(self)) {
        return 1;
    }

    // Now, we have enough memory
    if (i < self->size) {
        // There is data on the right, so we shift it
        memmove(self->data + (self->item_size * (i + 1)), self->data + (self->item_size * i), self->item_size * (self->size - i));
    }

    ++self->size;

    // We copy the new item in its place
    memcpy(array_get(self, i), ptr, self->item_size);

    return 0;
}


void array_remove(struct array* self, size_t i) {
    assert(i < self->size);

    // We may have data to move to the left (after i)
    if (i < self->size - 1) {
        memmove(self->data + i * self->item_size, self->data + (i + 1) * self->item_size, (self->size - i - 1) * self->item_size);
    }

    // We decrement the array's size
    --self->size;
}


size_t array_find(const struct array* self, const void* target) {
    for (size_t i = 0; i < self->size; ++i) {
        if (memcmp(self->data + (i * self->item_size), target, self->item_size) == 0) {
            return i;
        }
    }

    return self->size;
}

