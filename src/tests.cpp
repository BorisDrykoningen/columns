#include "array/array.h"
#include "grid/algorithms.h"

#include <gtest/gtest.h>


TEST(Array, Create) {
    array array;
    array_create(&array, sizeof(int));

    EXPECT_EQ(array.size, 0);
    EXPECT_EQ(array.item_size, sizeof(int));

    array_destroy(&array);
}


TEST(Array, Empty) {
    array array;
    array_create(&array, sizeof(int));

    EXPECT_TRUE(array_is_empty(&array));
    EXPECT_EQ(array_size(&array), 0);

    array_destroy(&array);
}


TEST(Array, PushBackOnce) {
    array array;
    array_create(&array, sizeof(int));
    const int fourty_two = 42;
    array_push_back(&array, &fourty_two);

    EXPECT_FALSE(array_is_empty(&array));
    EXPECT_EQ(array_size(&array), 1);
    EXPECT_EQ(array_get_const_typed(&array, 0, int), 42);

    array_destroy(&array);
}


TEST(Array, PushBackMultipleTimes) {
    array array;
    array_create(&array, sizeof(int));
    int to_add = 42;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    EXPECT_FALSE(array_is_empty(&array));
    EXPECT_EQ(array_size(&array), 4);
    EXPECT_EQ(array_get_const_typed(&array, 0, int), 42);
    EXPECT_EQ(array_get_const_typed(&array, 1, int), 43);
    EXPECT_EQ(array_get_const_typed(&array, 2, int), 44);
    EXPECT_EQ(array_get_const_typed(&array, 3, int), 45);

    array_destroy(&array);
}


TEST(Array, PushBackReallyLargeAmount) {
    const size_t large_amount = 10000;

    array array;
    array_create(&array, sizeof(size_t));

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(array_push_back(&array, &i), 0);
    }

    ASSERT_EQ(array_size(&array), large_amount);

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(array_get_const_typed(&array, i, size_t), i);
    }

    array_destroy(&array);
}


TEST(Array, PopBack) {
    array array;
    array_create(&array, sizeof(int));
    int to_add = 42;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    array_pop_back(&array);
    array_pop_back(&array);

    EXPECT_FALSE(array_is_empty(&array));
    EXPECT_EQ(array_size(&array), 2);
    EXPECT_EQ(array_get_const_typed(&array, 0, int), 42);
    EXPECT_EQ(array_get_const_typed(&array, 1, int), 43);

    array_pop_back(&array);
    array_pop_back(&array);

    EXPECT_TRUE(array_is_empty(&array));
    EXPECT_EQ(array_size(&array), 0);

    array_destroy(&array);
}


TEST(Array, PopBackUntilEmpty) {
    array array;
    array_create(&array, sizeof(int));
    int to_add = 42;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    ASSERT_FALSE(array_is_empty(&array));

    array_pop_back(&array);
    array_pop_back(&array);

    ASSERT_TRUE(array_is_empty(&array));

    array_destroy(&array);
}


TEST(Array, PopBackAlreadyEmpty) {
    array array;
    array_create(&array, sizeof(int));
    int to_add = 42;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    ASSERT_FALSE(array_is_empty(&array));

    array_pop_back(&array);
    array_pop_back(&array);

    ASSERT_TRUE(array_is_empty(&array));

    array_pop_back(&array);

    ASSERT_TRUE(array_is_empty(&array));

    array_destroy(&array);
}


TEST(Array, InsertAtTheEnd) {
    array array;
    array_create(&array, sizeof(size_t));

    size_t to_add = 0;
    array_insert(&array, 0, &to_add);
    ++to_add;
    array_insert(&array, 1, &to_add);

    ASSERT_EQ(2, array_size(&array));

    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(i, array_get_const_typed(&array, i, size_t));
    }

    array_destroy(&array);
}


TEST(Array, InsertReallyLargeAmountAtTheEnd) {
    array array;
    array_create(&array, sizeof(size_t));
    const size_t large_amount = 10000;

    for (size_t i = 0; i < large_amount; ++i) {
        array_insert(&array, i, &i);
    }

    ASSERT_EQ(array_size(&array), large_amount);

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(i, array_get_const_typed(&array, i, size_t));
    }

    array_destroy(&array);
}


TEST(Array, InsertInTheMiddle) {
    array array;
    array_create(&array, sizeof(size_t));

    size_t to_add = 0;
    array_push_back(&array, &to_add);
    to_add = 2;
    array_push_back(&array, &to_add);

    to_add = 1;
    array_insert(&array, 1, &to_add);

    ASSERT_EQ(3, array_size(&array));

    for (size_t i = 0; i < 3; ++i) {
        EXPECT_EQ(i, array_get_const_typed(&array, i, size_t));
    }

    array_destroy(&array);
}


TEST(Array, InsertReallyLargeAmountNearToTheEnd) {
    array array;
    array_create(&array, sizeof(size_t));
    const size_t large_amount = 10000;

    size_t to_add = 0;
    array_push_back(&array, &to_add);
    to_add = large_amount - 1;
    array_push_back(&array, &to_add);

    for (size_t i = 1; i < large_amount - 1; ++i) {
        array_insert(&array, i, &i);
    }

    ASSERT_EQ(large_amount, array_size(&array));

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(i, array_get_const_typed(&array, i, size_t));
    }

    array_destroy(&array);
}


TEST(Array, InsertReallyLargeAmountNearToTheBeginning) {
    array array;
    array_create(&array, sizeof(size_t));
    const size_t large_amount = 10000;

    size_t to_add = 0;
    array_push_back(&array, &to_add);
    to_add = large_amount - 1;
    array_push_back(&array, &to_add);

    for (size_t i = large_amount - 2; i > 0; --i) {
        array_insert(&array, 1, &i);
    }

    ASSERT_EQ(large_amount, array_size(&array));

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(i, array_get_const_typed(&array, i, size_t));
    }

    array_destroy(&array);
}


TEST(Array, RemoveAtTheEnd) {
    array array;
    array_create(&array, sizeof(size_t));
    size_t to_add = 0;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    array_destroy(&array);
}


TEST(Array, RemoveReallyLargeAmountAtTheEnd) {
    const size_t large_amount = 10000;
    array array;
    array_create(&array, sizeof(size_t));
    for (size_t i = 0; i < large_amount; ++i) {
        array_push_back(&array, &i);
    }

    ASSERT_EQ(array_size(&array), large_amount);

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(array_size(&array), large_amount - i);
        EXPECT_EQ(array_get_const_typed(&array, large_amount - i - 1, size_t), large_amount - i - 1);
        array_remove(&array, large_amount - i - 1);
    }
    EXPECT_EQ(array_size(&array), 0);
    EXPECT_TRUE(array_is_empty(&array));

    array_destroy(&array);
}


TEST(Array, RemoveInTheMiddle) {
    array array;
    array_create(&array, sizeof(size_t));
    size_t to_add = 0;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    array_remove(&array, 1);

    ASSERT_EQ(array_size(&array), 2);
    EXPECT_EQ(array_get_typed(&array, 0, size_t), 0);
    EXPECT_EQ(array_get_typed(&array, 1, size_t), 2);

    array_destroy(&array);
}


TEST(Array, RemoveAtTheBeginning) {
    array array;
    array_create(&array, sizeof(size_t));
    size_t to_add = 0;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);
    ++to_add;
    array_push_back(&array, &to_add);

    array_remove(&array, 0);

    ASSERT_EQ(array_size(&array), 2);
    EXPECT_EQ(array_get_typed(&array, 0, size_t), 1);
    EXPECT_EQ(array_get_typed(&array, 1, size_t), 2);

    array_destroy(&array);
}


TEST(Array, RemoveReallyLargeAmountAtTheBeginning) {
    const size_t large_amount = 10000;
    array array;
    array_create(&array, sizeof(size_t));
    for (size_t i = 0; i < large_amount; ++i) {
        array_push_back(&array, &i);
    }

    ASSERT_EQ(array_size(&array), large_amount);

    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(array_size(&array), large_amount - i);
        EXPECT_EQ(array_get_const_typed(&array, 0, size_t), i);
        array_remove(&array, 0);
    }
    EXPECT_EQ(array_size(&array), 0);
    EXPECT_TRUE(array_is_empty(&array));

    array_destroy(&array);
}


TEST(Array, CreateCopyEmpty) {
    struct array array;
    struct array copy;
    array_create(&array, sizeof(size_t));

    EXPECT_TRUE(array_is_empty(&array));

    array_create_copy(&copy, &array);

    EXPECT_TRUE(array_is_empty(&copy));

    array_destroy(&copy);
    array_destroy(&array);
}


TEST(Array, CreateCopy) {
    const size_t large_amount = 10000;

    struct array array;
    struct array copy;
    array_create(&array, sizeof(size_t));

    for (size_t i = 0; i < large_amount; ++i) {
        array_push_back(&array, &i);
    }

    array_create_copy(&copy, &array);

    ASSERT_EQ(array_size(&array), array_size(&copy));
    for (size_t i = 0; i < large_amount; ++i) {
        EXPECT_EQ(
            array_get_const_typed(&array, i, size_t),
            array_get_const_typed(&copy,  i, size_t)
        );
    }

    array_destroy(&copy);
    array_destroy(&array);
}


TEST(Array, NoSharedMemory) {
    struct array array;
    struct array copy;
    array_create(&array, sizeof(size_t));

    const size_t zero = 0;
    array_push_back(&array, &zero);

    array_create_copy(&copy, &array);

    ++array_get_typed(&array, 0, size_t);

    EXPECT_NE(
        array_get_const_typed(&array, 0, size_t),
        array_get_const_typed(&copy,  0, size_t)
    );

    array_destroy(&copy);
    array_destroy(&array);
}


TEST(Array, FindEmpty) {
    struct array array;
    array_create(&array, sizeof(size_t));

    const size_t fourty_two = 42;
    EXPECT_TRUE(array_find(&array, &fourty_two) >= array_size(&array));

    array_destroy(&array);
}


TEST(Array, FindNotFound) {
    const size_t large_amount = 10000;

    struct array array;
    array_create(&array, sizeof(size_t));

    for (size_t i = 0; i < large_amount; ++i) {
        array_push_back(&array, &i);
    }

    EXPECT_TRUE(array_find(&array, &large_amount) >= large_amount);

    array_destroy(&array);
}


TEST(Array, FindFound) {
    const size_t large_amount = 10000;
    const size_t to_find = 2000;

    struct array array;
    array_create(&array, sizeof(size_t));

    for (size_t i = 0; i < large_amount; ++i) {
        array_push_back(&array, &i);
    }

    EXPECT_TRUE(array_find(&array, &to_find) < large_amount);

    array_destroy(&array);
}


TEST(Grid, Create) {
    grid grid;
    // Arbitrary size
    grid_create(&grid, 4);

    EXPECT_GT(grid.capacity, 0);
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 0);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_EQ(grid.width, 4);
    EXPECT_NE(grid.content, nullptr);

    grid_destroy(&grid);
}


TEST(Grid, SetNoMalloc) {
    grid grid;
    grid_create(&grid, 4);
    grid_set(&grid, 0, 2, 5);

    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 1);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_EQ(grid_get(&grid, 0, 0), 0);
    EXPECT_EQ(grid_get(&grid, 0, 1), 0);
    EXPECT_EQ(grid_get(&grid, 0, 2), 5);
    EXPECT_EQ(grid_get(&grid, 0, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, SetMalloc) {
    grid grid;
    grid_create(&grid, 4);
    grid_set(&grid, 128, 0, 7);

    EXPECT_EQ(grid_column_height(&grid, 0), 129);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 0);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_GE(grid.capacity, 129);
    EXPECT_EQ(grid_get(&grid, 128, 0), 7);
    EXPECT_EQ(grid_get(&grid, 128, 1), 0);
    EXPECT_EQ(grid_get(&grid, 128, 2), 0);
    EXPECT_EQ(grid_get(&grid, 128, 3), 0);

    grid_destroy(&grid);
}


// A case that was detected too late in development :/
TEST(Grid, VerticalSets) {
    grid grid;
    grid_create(&grid, 5);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    grid_set(&grid, 0, 2, 2);
    grid_set(&grid, 1, 2, 1);
    grid_set(&grid, 2, 2, 1);

    const int stack[] = {1, 2, 3};
    const size_t height = grid_column_height(&grid, 2);
    grid_set(&grid, height,     2, stack[0]);
    grid_set(&grid, height + 1, 2, stack[1]);
    grid_set(&grid, height + 2, 2, stack[2]);

    grid_destroy(&grid);
}


TEST(Grid, GetOutOfRange) {
    grid grid;
    grid_create(&grid, 4);

    EXPECT_EQ(grid_get(&grid, 65535, 0), 0);

    grid_destroy(&grid);
}


TEST(Grid, ColumnHeightEmpty) {
    grid grid;
    grid_create(&grid, 4);

    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 0);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ColumnHeightFilled) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 1 | 1 | 0 |
    // | 1 | 1 | 0 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 1, 2, 1);

    EXPECT_EQ(grid_column_height(&grid, 0), 1);
    EXPECT_EQ(grid_column_height(&grid, 1), 2);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ColumnHeightFilledInChaoticOrder) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 1 | 0 | 0 | 0 |
    // | 0 | 1 | 1 | 0 |
    // | 0 | 0 | 1 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 1 | 1 | 1 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 4, 0, 1);
    grid_set(&grid, 3, 1, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 2, 2, 1);
    grid_set(&grid, 3, 2, 1);
    grid_set(&grid, 0, 2, 1);

    EXPECT_EQ(grid_column_height(&grid, 0), 5);
    EXPECT_EQ(grid_column_height(&grid, 1), 4);
    EXPECT_EQ(grid_column_height(&grid, 2), 4);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ColumnHeightAfterRemove) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 0 | 1 | 0 |
    // | 1 | 1 | 0 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 1, 2, 1);

    grid_remove(&grid, 1, 1);

    EXPECT_EQ(grid_column_height(&grid, 0), 1);
    EXPECT_EQ(grid_column_height(&grid, 1), 1);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ColumnHeightAfterRemoveWithGaps) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 1 | 0 |
    // | 1 | 1 | 0 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 1, 2, 1);
    grid_set(&grid, 2, 1, 1);

    grid_remove(&grid, 2, 1);

    EXPECT_EQ(grid_column_height(&grid, 0), 1);
    EXPECT_EQ(grid_column_height(&grid, 1), 1);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ApplyGravityEmpty) {
    grid grid;
    grid_create(&grid, 4);

    grid_apply_gravity(&grid);

    for (size_t row = 0; row < 10; ++row) {
        for (size_t col = 0; col < grid.width; ++col) {
            EXPECT_EQ(grid_get(&grid, row, col), 0);
        }
    }

    grid_destroy(&grid);
}


TEST(Grid, ApplyGravityNoFall) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 0 | 1 | 0 |
    // | 0 | 1 | 1 | 0 |
    // | 1 | 1 | 1 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 0, 2, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 1, 2, 1);
    grid_set(&grid, 2, 2, 1);

    grid_apply_gravity(&grid);

    for (size_t col = 0; col < 3; ++col) {
        EXPECT_EQ(grid_column_height(&grid, col), col + 1);
    }
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ApplyGravityFallOne) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 0 | 1 | 0 |
    // | 0 | 1 | 0 | 0 |
    // | 1 | 0 | 1 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 2, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 2, 2, 1);

    grid_apply_gravity(&grid);

    EXPECT_EQ(grid_column_height(&grid, 0), 1);
    EXPECT_EQ(grid_column_height(&grid, 1), 1);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, ApplyGravityFall) {
    grid grid;
    grid_create(&grid, 4);

    //        ...
    // | 0 | 0 | 0 | 1 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 0 | 0 |
    // | 0 | 0 | 1 | 0 |
    // | 0 | 1 | 0 | 0 |
    // | 1 | 0 | 0 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 2, 2, 1);
    grid_set(&grid, 9, 3, 1);

    grid_apply_gravity(&grid);

    for (size_t col = 0; col < 4; ++col) {
        EXPECT_EQ(grid_column_height(&grid, col), 1);
    }

    grid_destroy(&grid);
}


TEST(Grid, PlayEmpty) {
    grid grid;
    grid_create(&grid, 4);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    EXPECT_EQ(score, 0);
    EXPECT_EQ(grid_get(&grid, 0, 2), 1);
    EXPECT_EQ(grid_get(&grid, 1, 2), 2);
    EXPECT_EQ(grid_get(&grid, 2, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 3);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, PlayScoreZero) {
    grid grid;
    grid_create(&grid, 4);

    grid_set(&grid, 0, 1, 4);
    grid_set(&grid, 1, 1, 5);
    grid_set(&grid, 2, 1, 6);
    grid_set(&grid, 3, 1, 7);

    grid_set(&grid, 0, 3, 8);
    grid_set(&grid, 1, 3, 9);
    grid_set(&grid, 2, 3, 1);
    grid_set(&grid, 3, 3, 2);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    EXPECT_EQ(score, 0);
    // Column 1
    EXPECT_EQ(grid_get(&grid, 0, 1), 4);
    EXPECT_EQ(grid_get(&grid, 1, 1), 5);
    EXPECT_EQ(grid_get(&grid, 2, 1), 6);
    EXPECT_EQ(grid_get(&grid, 3, 1), 7);
    // Column 3
    EXPECT_EQ(grid_get(&grid, 0, 3), 8);
    EXPECT_EQ(grid_get(&grid, 1, 3), 9);
    EXPECT_EQ(grid_get(&grid, 2, 3), 1);
    EXPECT_EQ(grid_get(&grid, 3, 3), 2);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 1);
    EXPECT_EQ(grid_get(&grid, 1, 2), 2);
    EXPECT_EQ(grid_get(&grid, 2, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 4);
    EXPECT_EQ(grid_column_height(&grid, 2), 3);
    EXPECT_EQ(grid_column_height(&grid, 3), 4);

    grid_destroy(&grid);
}


TEST(Grid, HorizontalMatch) {
    grid grid;
    grid_create(&grid, 4);

    // | 0 | 0 | 3 | 0 |
    // | 0 | 1 | 2 | 0 |
    // | 0 | 1 | 1 | 1 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 0, 3, 1);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    // | 0 | 0 | 3 | 0 |
    // | 0 | 1 | 2 | 0 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    EXPECT_EQ(score, 1);
    // Column 1
    EXPECT_EQ(grid_get(&grid, 0, 1), 1);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    EXPECT_EQ(grid_get(&grid, 1, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 1);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);

    grid_destroy(&grid);
}


TEST(Grid, LongHorizontalMatch) {
    grid grid;
    grid_create(&grid, 5);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 1 | 2 | 0 | 0 |
    // | 1 | 1 | 1 | 1 | 1 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 1, 1, 1);
    grid_set(&grid, 0, 3, 1);
    grid_set(&grid, 0, 4, 1);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 1 | 2 | 0 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    EXPECT_EQ(score, 3);
    // Column 1
    EXPECT_EQ(grid_get(&grid, 0, 1), 1);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    EXPECT_EQ(grid_get(&grid, 1, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 1);
    EXPECT_EQ(grid_column_height(&grid, 2), 2);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_EQ(grid_column_height(&grid, 4), 0);

    grid_destroy(&grid);
}


TEST(Grid, VerticalMatch) {
    grid grid;
    grid_create(&grid, 5);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 1 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    grid_set(&grid, 0, 2, 2);
    grid_set(&grid, 1, 2, 1);
    grid_set(&grid, 2, 2, 1);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // | 0 | 0 | 2 | 0 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    EXPECT_EQ(score, 1);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    EXPECT_EQ(grid_get(&grid, 1, 2), 2);
    EXPECT_EQ(grid_get(&grid, 2, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 3);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_EQ(grid_column_height(&grid, 4), 0);

    grid_destroy(&grid);
}


TEST(Grid, TL2BRMatch) {
    grid grid;
    grid_create(&grid, 5);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 1 | 2 | 0 | 0 |
    // | 0 | 3 | 1 | 0 | 0 |
    // | 0 | 4 | 2 | 1 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    grid_set(&grid, 0, 1, 4);
    grid_set(&grid, 1, 1, 3);
    grid_set(&grid, 2, 1, 1);
    grid_set(&grid, 0, 2, 2);
    grid_set(&grid, 0, 3, 1);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 3 | 2 | 0 | 0 |
    // | 0 | 4 | 2 | 0 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    EXPECT_EQ(score, 1);
    // Column 1
    EXPECT_EQ(grid_get(&grid, 0, 1), 4);
    EXPECT_EQ(grid_get(&grid, 1, 1), 3);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    EXPECT_EQ(grid_get(&grid, 1, 2), 2);
    EXPECT_EQ(grid_get(&grid, 2, 2), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 2);
    EXPECT_EQ(grid_column_height(&grid, 2), 3);
    EXPECT_EQ(grid_column_height(&grid, 3), 0);
    EXPECT_EQ(grid_column_height(&grid, 4), 0);

    grid_destroy(&grid);
}


TEST(Grid, BL2TRMatch) {
    grid grid;
    grid_create(&grid, 5);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 0 | 2 | 1 | 0 |
    // | 0 | 0 | 1 | 3 | 0 |
    // | 0 | 1 | 2 | 4 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 0, 2, 2);
    grid_set(&grid, 0, 3, 4);
    grid_set(&grid, 1, 3, 3);
    grid_set(&grid, 2, 3, 1);

    const int stack[] = {1, 2, 3};
    const int score = grid_play(&grid, stack, 2);

    // | 0 | 0 | 3 | 0 | 0 |
    // | 0 | 0 | 2 | 3 | 0 |
    // | 0 | 0 | 2 | 4 | 0 |
    // +---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 |
    EXPECT_EQ(score, 1);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    EXPECT_EQ(grid_get(&grid, 1, 2), 2);
    EXPECT_EQ(grid_get(&grid, 2, 2), 3);
    // Column 3
    EXPECT_EQ(grid_get(&grid, 0, 3), 4);
    EXPECT_EQ(grid_get(&grid, 1, 3), 3);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 3);
    EXPECT_EQ(grid_column_height(&grid, 3), 2);
    EXPECT_EQ(grid_column_height(&grid, 4), 0);

    grid_destroy(&grid);
}


TEST(Grid, ChainReaction) {
    grid grid;
    grid_create(&grid, 7);

    // | 0 | 0 | 3 | 3 | 0 | 0 | 0 |
    // | 0 | 0 | 2 | 2 | 0 | 0 | 0 |
    // | 0 | 3 | 1 | 4 | 0 | 0 | 0 |
    // | 1 | 1 | 2 | 2 | 0 | 2 | 2 |
    // +---+---+---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 1);
    grid_set(&grid, 0, 2, 2);
    grid_set(&grid, 0, 3, 2);
    grid_set(&grid, 0, 5, 2);
    grid_set(&grid, 0, 6, 2);
    grid_set(&grid, 1, 1, 3);
    grid_set(&grid, 1, 2, 1);
    grid_set(&grid, 1, 3, 4);
    grid_set(&grid, 2, 2, 2);
    grid_set(&grid, 2, 3, 2);
    grid_set(&grid, 3, 2, 3);
    grid_set(&grid, 3, 3, 3);

    const int stack[] = {2, 5, 6};
    const int score = grid_play(&grid, stack, 4);

    // | 0 | 0 | 2 | 4 | 0 | 0 | 0 |
    // +---+---+---+---+---+---+---+
    // | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
    EXPECT_EQ(score, 8);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 2);
    // Column 3
    EXPECT_EQ(grid_get(&grid, 0, 3), 4);
    // Column 4
    EXPECT_EQ(grid_get(&grid, 0, 4), 5);
    EXPECT_EQ(grid_get(&grid, 1, 4), 6);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 0);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 1);
    EXPECT_EQ(grid_column_height(&grid, 3), 2);
    EXPECT_EQ(grid_column_height(&grid, 4), 2);
    EXPECT_EQ(grid_column_height(&grid, 5), 0);
    EXPECT_EQ(grid_column_height(&grid, 6), 0);

    grid_destroy(&grid);
}


TEST(Grid, SingleStainOfMatches) {
    grid grid;
    grid_create(&grid, 4);

    // | 0 | 2 | 0 | 0 |
    // | 0 | 2 | 2 | 0 |
    // | 2 | 3 | 3 | 0 |
    // | 1 | 2 | 1 | 1 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    grid_set(&grid, 0, 0, 1);
    grid_set(&grid, 0, 1, 2);
    grid_set(&grid, 0, 2, 1);
    grid_set(&grid, 0, 3, 1);
    grid_set(&grid, 1, 0, 2);
    grid_set(&grid, 1, 1, 3);
    grid_set(&grid, 1, 2, 3);
    grid_set(&grid, 2, 1, 2);
    grid_set(&grid, 2, 2, 2);
    grid_set(&grid, 3, 1, 2);

    const int stack[] = {3, 5, 6};
    const int score = grid_play(&grid, stack, 3);

    // | 0 | 0 | 0 | 6 |
    // | 0 | 0 | 0 | 5 |
    // | 1 | 0 | 1 | 1 |
    // +---+---+---+---+
    // | 0 | 1 | 2 | 3 |
    EXPECT_EQ(score, 5);
    // Column 0
    EXPECT_EQ(grid_get(&grid, 0, 0), 1);
    // Column 2
    EXPECT_EQ(grid_get(&grid, 0, 2), 1);
    // Column 3
    EXPECT_EQ(grid_get(&grid, 0, 3), 1);
    EXPECT_EQ(grid_get(&grid, 1, 3), 5);
    EXPECT_EQ(grid_get(&grid, 2, 3), 6);
    // Checking whether there is nothing else
    EXPECT_EQ(grid_column_height(&grid, 0), 1);
    EXPECT_EQ(grid_column_height(&grid, 1), 0);
    EXPECT_EQ(grid_column_height(&grid, 2), 1);
    EXPECT_EQ(grid_column_height(&grid, 3), 3);

    grid_destroy(&grid);
}

