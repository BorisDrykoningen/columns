CC ?= gcc
CXX ?= g++
CC_ARGS = -std=c99 -Wall -Wextra -Wpedantic -gdwarf-4 -O0
OBJS = algorithms.o array.o grid.o

all: columns tests

columns: $(OBJS) main.o
	$(CC) $(CC_ARGS) $(OBJS) main.o -o columns

tests: $(OBJS) tests.o
	$(CXX) $(CC_ARGS) $(OBJS) tests.o -o tests -lgtest -lgtest_main

algorithms.o: src/array/array.h src/grid/grid.h src/grid/algorithms.c src/grid/algorithms.h
	$(CC) $(CC_ARGS) src/grid/algorithms.c -c

array.o: src/array/array.c src/array/array.h
	$(CC) $(CC_ARGS) src/array/array.c -c

grid.o: src/array/array.h src/grid/grid.c src/grid/grid.h
	$(CC) $(CC_ARGS) src/grid/algorithms.c src/grid/grid.c -c

main.o: src/main.c src/array/array.h src/grid/grid.h src/grid/algorithms.h
	$(CC) $(CC_ARGS) src/main.c -c

tests.o: src/tests.cpp src/array/array.h src/grid/grid.h
	$(CXX) $(CXX_ARGS) src/tests.cpp -c

clean:
	rm *.o columns tests

